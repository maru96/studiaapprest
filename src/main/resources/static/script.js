document.addEventListener('DOMContentLoaded', () => {
    getAllStudents();
});

function getAllStudents() {
    fetch("http://localhost:8080/students")
        .then((response) => {
            if (response.status !== 200) {
                return Promise.reject('Coś poszło nie tak!');
            }
            return response.json();
        })
        .then((data) => {
            showTable(data);
        })
        .catch((error) => {
            console.log(error);
        });
}

function showTable(response) {
    var main = document.getElementById('main');
    var content = "<table border='1'> <thead> <tr> <th> Imię</th>" +
        "<th>Nazwisko</th><th>Średnia</th><th></th><th></th></tr></thead><tbody>";
    for (var st in response) {
        var id = response[st].id;
        var name = response[st].name;
        var surname = response[st].surname;
        var average = response[st].average;

        var userData = `${id}, "${name}", "${surname}", ${average}`;

        content += "<tr'><td>" +
            name + "</td><td>" + surname +
            "</td><td>" + average + "</td>" +
            "<td onclick='setUserData("+userData+")'><b style='color: cadetblue'>Edytuj</b></td>" +
            "<td onclick='deleteStudent("+id+")'><b style='color: indianred'>Usuń</b></td></tr>";
    }
    content += "</tbody></table>";
    main.innerHTML = content;
}

function setUserData(id, name, surname, average) {
    var st = {}; //tworzymy obiekt z polami o wartościach pobranych z pól formularza

    document.getElementById('editStudent').style.visibility = 'visible';

    document.getElementById('id').value = id;
    document.getElementById('firstName').value = name;
    document.getElementById('lastName').value = surname;
    document.getElementById('average').value = average;
}

function editStudent() {
    var st = {}; //tworzymy obiekt z polami o wartościach pobranych z pól formularza
    st.id = document.getElementById('id').value;
    st.name = document.getElementById('firstName').value;
    st.surname = document.getElementById('lastName').value;
    st.average = document.getElementById('average').value;

    fetch(`http://localhost:8080/students/${st.id}`, {
        method: "put",
        body: JSON.stringify(st),
        headers: {"Content-type": "application/json; charset=UTF-8"}
    })
        .then(response => response.json())
        .then(response => {
            window.location.href = "/";
        });
}

function add() {
    var st = {}; //tworzymy obiekt z polami o wartościach pobranych z pól formularza
    st.id = document.getElementById('id').value;
    st.name = document.getElementById('firstName').value;
    st.surname = document.getElementById('lastName').value;
    st.average = document.getElementById('average').value;
    console.log("dodaj")
    console.log(st)
    fetch("http://localhost:8080/students/add", {
        method: "post",
        body: JSON.stringify(st),
        headers: {"Content-type": "application/json; charset=UTF-8"}
    })
        .then(response => response.json())
        .then(response => {
            console.log("Dodano studenta:");
            console.log(response);
            window.location.href = "/";
        });
}

function deleteStudent(id)
{
    fetch(`http://localhost:8080/students/${id}`, {
        method: "delete",
        headers: {"Content-type": "application/json; charset=UTF-8"}
    })
    .then(response => window.location.href = "");
}
