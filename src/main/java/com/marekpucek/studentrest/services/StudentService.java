package com.marekpucek.studentrest.services;

import com.marekpucek.studentrest.entities.Student;
import com.marekpucek.studentrest.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService
{
    @Autowired
    private StudentRepository studentRepository;
    List<Student> students;

    public List<Student> getStudentList()
    {
        return (List<Student>) studentRepository.findAll();
    }

    public List<Student> addStudent(Student student)
    {
        studentRepository.save(student);

        return getStudentList();
    }

    public List<Student> deleteStudent(long id)
    {
        studentRepository.deleteById(id);

        return getStudentList();
    }

    public Student getStudent(long id) throws Exception
    {
        Optional<Student> student = studentRepository.findById(id);

        if (!student.isPresent())
            throw new Exception("Student not found");

        return student.get();
    }

    public List<Student> updateStudent(Student st, long id) throws Exception
    {
        Student student = this.getStudent(id);

        student.setName(st.getName());
        student.setSurname(st.getSurname());
        student.setAverage(st.getAverage());

        studentRepository.save(student);

        return getStudentList();
    }

}

