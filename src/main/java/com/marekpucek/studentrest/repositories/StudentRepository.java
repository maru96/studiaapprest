package com.marekpucek.studentrest.repositories;

import com.marekpucek.studentrest.entities.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, Long> {
}
