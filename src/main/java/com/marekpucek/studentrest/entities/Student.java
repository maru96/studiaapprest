package com.marekpucek.studentrest.entities;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;

@Entity
@Table(name = "Student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonView
    private String name;

    @JsonView
    private String surname;

    @JsonView
    private double average;

    public Student() {
    }

    public Student(String name) {
        this.name = name;
    }

    public Student(String name, String surname, double average) {
        this.name = name;
        this.surname = surname;
        this.average = average;
    }

    public Student(Long userid, String name, String surname, double average) {
        this.id = userid;
        this.name = name;
        this.surname = surname;
        this.average = average;
    }

    public Long getUserid() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public double getAverage() {
        return average;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAverage(double average) {
        this.average = average;
    }
}
