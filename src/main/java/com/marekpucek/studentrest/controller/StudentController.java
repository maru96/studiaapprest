package com.marekpucek.studentrest.controller;

import com.marekpucek.studentrest.entities.Student;
import com.marekpucek.studentrest.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;


@RestController
public class StudentController
{
    @Autowired
    private StudentService studentService;

    @GetMapping("/students")
    public List<Student> getAll()
    {
        return this.studentService.getStudentList();
    }

    @GetMapping("/students/{id}")
    public Student getStudent(@PathVariable long id) throws Exception
    {
        return this.studentService.getStudent(id);
    }

    @PostMapping("/students/add")
    public List<Student> addStudent(@RequestBody Student student)
    {
        return this.studentService.addStudent(student);
    }

    @DeleteMapping("/students/{id}")
    public List<Student> deleteStudent(@PathVariable long id)
    {
        return this.studentService.deleteStudent(id);
    }

    @PutMapping(value = "/students/{id}")
    public List<Student> updateStudent(@PathVariable("id") long id,
                                       @RequestBody Student st) throws Exception
    {
        return this.studentService.updateStudent(st, id);
    }

    @RequestMapping(value = "/")
    public String getRootContent() throws IOException
    {
        return getResource("src\\main\\resources\\static\\index.html");
    }

    private String getResource(String filePath) throws FileNotFoundException
    {
        Scanner fileReader = new Scanner(new File(filePath));
        StringBuilder content = new StringBuilder();
        while (fileReader.hasNextLine())
        {
            content.append(fileReader.nextLine());
        }
        return content.toString();
    }

}
